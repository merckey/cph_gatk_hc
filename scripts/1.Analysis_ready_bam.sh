#
SAMPLEID=$1
READ1=/cph_wex/fastqs/$SAMPLEID/$SAMPLEID.1.fastq
READ2=/cph_wex/fastqs/$SAMPLEID/$SAMPLEID.2.fastq
#
THREADS=4
REF=/cph_wex/ref/ref.fasta
JAVA8=/opt/jdk1.8.0_45/bin/java

###
# ALIGNMENT
###

# create index
mkdir -p /cph_wex/ref/bwa_index/
BWA_INDEX=/cph_wex/ref/bwa_index/chr20
if [ ! -e $BWA_INDEX.amb ]; then
	echo "Build index files for BWA..."
	/usr/local/bin/bwa index -p $BWA_INDEX -a bwtsw $REF
fi

# align reads to reference genome
BAMDIR=/cph_wex/bams/$SAMPLEID/
mkdir -p $BAMDIR
echo "Start align read to reference genome..."
RG="@RG\tID:$SAMPLEID\tSM:$SAMPLEID\tPL:illumina"
/usr/local/bin/bwa mem \
-t $THREADS \
-R $RG \
$BWA_INDEX \
$READ1 \
$READ2 \
| samtools view -Sbh - > $BAMDIR$SAMPLEID.bam

# sort the bam
echo "Sorting the bam file by coordinates..."
$JAVA8 -jar /opt/picard.jar SortSam \
SORT_ORDER=coordinate \
CREATE_INDEX=true \
INPUT=$BAMDIR$SAMPLEID.bam \
OUTPUT=$BAMDIR$SAMPLEID.sorted.bam

# mark duplicates
echo "Mark duplicates..."
$JAVA8 -jar /opt/picard.jar MarkDuplicates \
ASSUME_SORT_ORDER=coordinate \
CREATE_INDEX=true \
REMOVE_DUPLICATES=FALSE \
READ_NAME_REGEX="[a-zA-Z0-9]+:[0-9]:([0-9]+):([0-9]+):([0-9]+).*" \
INPUT=$BAMDIR$SAMPLEID.sorted.bam \
OUTPUT=$BAMDIR$SAMPLEID.sorted.mark_dup.bam \
M=$BAMDIR$SAMPLEID.mark_dup.metrics

# IndelRealigner
GATK=/opt/GATK3.6/GenomeAnalysisTK.jar
KNOWINDEL1="/cph_wex/databases/1000G_phase1.indels.b37.20.vcf"
KNOWINDEL2="/cph_wex/databases/Mills_and_1000G_gold_standard.indels.b37.20.vcf"
echo "Performing indel realignment: creating targets..."
$JAVA8 -jar $GATK -T RealignerTargetCreator \
-R $REF \
-known $KNOWINDEL1 \
-known $KNOWINDEL2 \
-I $BAMDIR$SAMPLEID.sorted.mark_dup.bam \
-o $BAMDIR$SAMPLEID.realign.list

echo "Performing indel realignment: realign targets..."
$JAVA8 -jar $GATK -T IndelRealigner \
-R $REF \
-known $KNOWINDEL1 \
-known $KNOWINDEL2 \
-I $BAMDIR$SAMPLEID.sorted.mark_dup.bam \
-targetIntervals $BAMDIR$SAMPLEID.realign.list \
-o $BAMDIR$SAMPLEID.sorted.mark_dup.realigned.bam

echo "nPerforming BQSR: Builds recalibration model ..."
DBSNP=/cph_wex/databases/dbsnp.vcf
$JAVA8 -jar $GATK -T BaseRecalibrator \
-R $REF \
-knownSites $KNOWINDEL1 \
-knownSites $KNOWINDEL2 \
-knownSites $DBSNP \
-I $BAMDIR$SAMPLEID.sorted.mark_dup.realigned.bam \
-o $BAMDIR$SAMPLEID.recal.table

echo "Performing BQSR: write recalibrated file ..."
$JAVA8 -jar $GATK -T PrintReads \
-R $REF \
-I $BAMDIR$SAMPLEID.sorted.mark_dup.realigned.bam \
-BQSR $BAMDIR$SAMPLEID.recal.table \
-o $BAMDIR$SAMPLEID.sorted.mark_dup.realigned.bqsr.bam

$JAVA8 -jar $GATK -T BaseRecalibrator \
-R $REF \
-knownSites $KNOWINDEL1 \
-knownSites $KNOWINDEL2 \
-knownSites $DBSNP \
-I $BAMDIR$SAMPLEID.sorted.mark_dup.realigned.bqsr.bam \
-BQSR $BAMDIR$SAMPLEID.recal.table \
-o $BAMDIR$SAMPLEID.after_recal.table

$JAVA8 -jar $GATK -T AnalyzeCovariates \
-R $REF \
-before $BAMDIR$SAMPLEID.recal.table \
-after $BAMDIR$SAMPLEID.after_recal.table \
-plots $BAMDIR"recal_plots.pdf"

echo "Call variants use Unified Genotyper..."
VCFDIR=/cph_wex/vcfs/$SAMPLEID
mkdir -p $VCFDIR
$JAVA8 -jar $GATK -T UnifiedGenotyper \
-R $REF \
-I $BAMDIR$SAMPLEID.sorted.mark_dup.realigned.bqsr.bam \
-o $VCFDIR/$SAMPLEID.UG.vcf \
-glm BOTH \
-L 20:10,000,000-10,200,000

echo "Call variants use HaplotypeCaller..."
$JAVA8 -jar $GATK -T HaplotypeCaller \
-R $REF \
-I $BAMDIR$SAMPLEID.sorted.mark_dup.realigned.bqsr.bam \
-o $VCFDIR/$SAMPLEID.HC.vcf \
-L /cph_wex/ref/intervals.list

# 20:10,002,434-10,002,506 no snp after HC
$JAVA8 -jar $GATK -T HaplotypeCaller \
-R $REF \
-I $BAMDIR$SAMPLEID.sorted.mark_dup.realigned.bqsr.bam \
-o $VCFDIR/$SAMPLEID.HCdebug.vcf \
-bamout $VCFDIR/$SAMPLEID.HCdebug.bam \
-forceActive -disableOptimizations \
-L /cph_wex/ref/intervals.list -ip 100

echo "Hard filtering low quality snps..."
echo "Step1: Extract SNP from the raw call subset"
$JAVA8 -jar $GATK \
-T SelectVariants \
-R $REF \
-V $VCFDIR/$SAMPLEID.HC.vcf \
-selectType SNP \
-o $VCFDIR/$SAMPLEID.HC.SNP.vcf

echo "Step2: Apply filter parameters for SNPs."
$JAVA8 -jar $GATK \
-T VariantFiltration \
-R $REF \
-o $VCFDIR/$SAMPLEID.HC.SNP.filtered.vcf \
--variant $VCFDIR/$SAMPLEID.HC.SNP.vcf \
--filterExpression "QD < 2.0 || FS > 60.0 || MQ < 40.0 || MQRankSum < -12.5 || ReadPosRankSum < -8.0" \
--filterName "lowQual"

echo "Step3: Extract INDEL from the raw call subset"
$JAVA8 -jar $GATK \
-T SelectVariants \
-R $REF \
-V $VCFDIR/$SAMPLEID.HC.vcf \
-selectType INDEL  \
-o $VCFDIR/$SAMPLEID.HC.INDEL.vcf

echo "Step4: Apply filter parameters for INDELs."
$JAVA8 -jar $GATK \
-T VariantFiltration \
-R $REF \
-o $VCFDIR/$SAMPLEID.HC.INDEL.filtered.vcf \
--variant $VCFDIR/$SAMPLEID.HC.INDEL.vcf \
--filterExpression "QD < 2.0 || FS > 200.0 || ReadPosRankSum < -20.0" \
--filterName "lowQual"
