# Variant discovery using Whole Genome Sequencing (WGS)

Lab on Google Cloud VM:
  * Google Account Login and direct you web browser to [Google Cloud Platform](https://console.cloud.google.com).
  * Access the VM through SSH.
  * Start your VM instance if its current status is "STOP".
  * Change the IP address in the SFTP client to the current VM external IP.

Tools used in this lab:
  1. [FastQC](http://www.bioinformatics.babraham.ac.uk/projects/fastqc/)
  1. [BWA](http://bio-bwa.sourceforge.net/bwa.shtml)
  2. [Samtools](https://github.com/samtools/samtools)
  3. [PICARD](https://broadinstitute.github.io/picard/)
  4. [GATK3.6](https://software.broadinstitute.org/gatk/)

## Preparation

  __You only need to run the preparation once.__

  * Download/update the docker image. It will download the image and will get updated if we add more tools for the lab in the future.
  ```
  # download docker image
  docker pull merckey/cph738_lab:wex
  # check current image available on you VM
  docker images

  REPOSITORY           TAG                 IMAGE ID            CREATED             SIZE
  merckey/cph738_lab   wex                 4a2bb442176e        2 years ago         1.47GB
  merckey/cph738_lab   r_custom            b9ab309c6e71        2 years ago         1.06GB
  ```

  * Download the data for lab/homework.
  ```
  git clone https://merckey@bitbucket.org/merckey/cph_gatk_hc.git
  ```

## Access lab/hw working env

  * To start the docker container, run the following command. It will start the operating system for the lab.  
  ```
  docker run -it  -v ~/cph_gatk_hc/:/cph_wex -w /cph_wex  merckey/cph738_lab:wex
  # You should be able to run samtools if your are in the container
  samtools
  # Output
  Program: samtools (Tools for alignments in the SAM format)
  Version: 0.1.19-44428cd
  Usage:   samtools <command> [options]
  Command: view        SAM<->BAM conversion
  ......
  ```

  * Make sure your are in the folder /cph_wex/. To check your current path, use `pwd`. You can change your directory use `cd`.
  ```
  # Show the current path
  pwd
  # Output should be:
  /cph_wex
  # if not change the directory to folder /cph_wex
  cd /cph_wex
  ```

## Lab/hw data description

  * __Whole genome sequencing (WGS) data.__ Check the fastq files in the 'fastqs' folder. There are 2 samples NA12877 (for lab) and NA12878(for homework). The WGS data only contain a subset of the chromosome 20. Since the data were generated using pair-end sequencing, you will find 2 fastq files for each sample in folder ```fastqs/NA12877```.
  ```
  ls fastqs/NA12877/
  NA12877.1.fastq # read1
  NA12877.2.fastq # read2
  ```
  * __Reference genome for Human.__ We will use the b37 version of the human reference genome. Only the chromosome 20 is provided for the lab and homework.
  ```
  ls ref/
  ref.dict            # genome reference
  ref.fasta           # sequence dictionary
  ref.fasta.fai       # fasta index
  ```

  * __SNP and INDEL databases__
  ```
  ls databases/
  1000G_phase1.indels.b37.20.vcf
  1000G_phase1.indels.b37.20.vcf.idx
  Mills_and_1000G_gold_standard.indels.b37.20.vcf
  Mills_and_1000G_gold_standard.indels.b37.20.vcf.idx
  dbsnp.vcf
  dbsnp.vcf.idx
  ```

## Pipeline steps
The pipeline for variants discovery using GATK has 2 parts. Part 1 is to preprocess the sequencing data resulting in analysis-ready bam file. Part2 will produce variants callset and do filtering.

### 1. Preprocessing

#### 1. Map reads to reference genome.
  Take the short read and try to find a placement from them in the genome. BWA use suffix tree which is super fast, but given the amount of reads generated from WGS, it takes a long time to run. That's why we only used a subset for the demonstration. Once you map it to the position, alignment will try to align it as best as possible.

  __Read group information.__ The read group information is key for downstream GATK functionality. The GATK will not work without a read group tag. Make sure to enter as much metadata as you know about your data in the read group fields provided. In the downstream analysis, GATK need to know which sample this reads came from such as lane, unit, and platform. You may have one sample that sequenced over 20 different machines and you want to combine them into one file so you need to add each read group to each of them and combine.
  ```
  # create index for efficient accessing the reference genome
  bwa index -p $BWA_INDEX -a bwtsw $REF

  # align reads to reference genome
  RG="@RG\tID:$SAMPLEID\tSM:$SAMPLEID\tPL:illumina"
  /usr/local/bin/bwa mem \
  -t $THREADS \
  -R $RG \
  $BWA_INDEX \
  $READ1 \
  $READ2 \
  | samtools view -Sbh - > $BAMDIR$SAMPLEID.bam

  ```
  Expected output: ```sample.bam```

#### 2. Sort and Mark Duplicates.
  GATK expect reads to be sorted by the starting position. We use picard ```SortSam```.

  ```
  $JAVA8 -jar picard.jar SortSam \
  SORT_ORDER=coordinate \
  CREATE_INDEX=true \
  INPUT=$BAMDIR$SAMPLEID.bam \
  OUTPUT=$BAMDIR$SAMPLEID.sorted.bam
  ```
  Expected output: ```sample.sorted.bam```

  Duplicates may originate from DNA preparation method. The sequencer produces multiple reads for the same fragment or molecules. We don't want multiple copies of that molecule adding to that site and will cause biases that skew the calling results. ```MarkDuplicates``` will try to find duplications and flag it as duplicates.

  ```
  $JAVA8 -jar picard.jar MarkDuplicates \
  ASSUME_SORT_ORDER=coordinate \
  CREATE_INDEX=true \
  REMOVE_DUPLICATES=FALSE \
  READ_NAME_REGEX="[a-zA-Z0-9]+:[0-9]:([0-9]+):([0-9]+):([0-9]+).*" \
  INPUT=$BAMDIR$SAMPLEID.sorted.bam \
  OUTPUT=$BAMDIR$SAMPLEID.sorted.mark_dup.bam \
  M=$BAMDIR$SAMPLEID.mark_dup.metrics
  ```

#### 3. Indel Realignment
  The artifactual mismatches from the aligner can harm base quality recalibration and variant detection. This step will help improve the accuracy of several of the downstream processing steps.

  Step1: Identify regions needing realignment from Konw indels and aligned reads.

  From known indels – e.g. 1000 Genomes Project, dbSNP

  From the aligned reads – Indels seen in aligned read CIGARs – Sites suggesting a hidden indel

  ```
  $JAVA8 -jar $GATK -T RealignerTargetCreator \
  -R $REF \
  -known $KNOWINDEL1 \
  -known $KNOWINDEL2 \
  -I $BAMDIR$SAMPLEID.sorted.mark_dup.bam \
  -o $BAMDIR$SAMPLEID.realign.list
  The raw sequencing data are in the folder lab/raw. To show the contents in that folder, use `ls`.
  ```

  Expected output: ```sample.realign.list```

  Step2: Perform realignment
  Find the alternate concensus sequences best fits reads, score the concensus uses sum of quality scores of the mismatching bases. If it is sufficiently better than the original alignments, accept the proposed realignment.
  ```
  $JAVA8 -jar $GATK -T IndelRealigner \
  -R $REF \
  -known $KNOWINDEL1 \
  -known $KNOWINDEL2 \
  -I $BAMDIR$SAMPLEID.sorted.mark_dup.bam \
  -targetIntervals $BAMDIR$SAMPLEID.realign.list \
  -o $BAMDIR$SAMPLEID.sorted.mark_dup.realigned.bam
  ```
  Expected output: ```sample.sorted.mark_dup.realigned.bam```

  Notes: Overall the IndelRealigner is not quite as impactful as before due the re-assembly steps in the HaplotypeCaller, although it can still improve mapping of low quality reads and allow them to be included in the downstream BQSR and variant calling step.

#### 4. BQSR

Quality scores are critical for all the downstream analysis. High quality will give more weights in variant calling whereas low quality reads will have low weights.

  Step1: model the error modes and recalibrate qualities

  ```
  $JAVA8 -jar $GATK -T BaseRecalibrator \
  -R $REF \
  -knownSites $KNOWINDEL1 \
  -knownSites $KNOWINDEL2 \
  -knownSites $DBSNP \
  -I $BAMDIR$SAMPLEID.sorted.mark_dup.realigned.bam \
  -o $BAMDIR$SAMPLEID.recal.table
  ```

  Expected output: ```sample.recal.table```

  Step2: write the recalibrated data to file
  ```
  $JAVA8 -jar $GATK -T PrintReads \
  -R $REF \
  -I $BAMDIR$SAMPLEID.sorted.mark_dup.realigned.bam \
  -BQSR $BAMDIR$SAMPLEID.recal.table \
  -o $BAMDIR$SAMPLEID.sorted.mark_dup.realigned.bqsr.bam
  ```

  Expected output: ```sample.sorted.mark_dup.realigned.bqsr.bam```

  Step3: Evaluate how the bqsr perform (optional). Run BaseRecalibrator on recalibrated bam file and make plot.
  ```
  $JAVA8 -jar $GATK -T BaseRecalibrator \
  -R $REF \
  -knownSites $KNOWINDEL1 \
  -knownSites $KNOWINDEL2 \
  -knownSites $DBSNP \
  -I $BAMDIR$SAMPLEID.sorted.mark_dup.realigned.bqsr.bam \
  -BQSR $BAMDIR$SAMPLEID.recal.table \
  -o $BAMDIR$SAMPLEID.after_recal.table

  $JAVA8 -jar $GATK -T AnalyzeCovariates \
  -R $REF \
  -before $BAMDIR$SAMPLEID.recal.table \
  -after $BAMDIR$SAMPLEID.after_recal.table \
  -plots $BAMDIR"recal_plots.pdf"

  ```

### Variant discovery
#### 1. Haplotype caller
Sequencer produced noisy data. We need an algorithm to identify real variants from the sequencer error.
  ```
  $JAVA8 -jar $GATK -T HaplotypeCaller \
  -R $REF \
  -I $BAMDIR$SAMPLEID.sorted.mark_dup.realigned.bqsr.bam \
  -o $VCFDIR/$SAMPLEID.HC.vcf \
  -L /cph_wex/ref/intervals.list
  ```
#### 2. VariantFiltration
The raw callset is infested with false positives. There are 2 ways to filter the variants.
  1. hard filtering which requires time and expertise. (VariantFiltration)
  2. learn which the filter should be from the data. (VQSR)

We will use GATK hard filtering app to remove low quality SNPs and INDELs.[HOW-TO](http://gatkforums.broadinstitute.org/gatk/discussion/2806/howto-apply-hard-filters-to-a-call-set)

  Step1: Extract SNP from the raw call subset
  ```
  $JAVA8 -jar $GATK \
  -T SelectVariants \
  -R $REF \
  -V $VCFDIR/$SAMPLEID.HC.vcf \
  -selectType SNP \
  -o $VCFDIR/$SAMPLEID.HC.SNP.vcf
  ```

  Step2: Apply filter parameters for SNPs.

    * QualByDepth(QD): This annotation puts the variant confidence QUAL score into perspective by normalizing for the amount of coverage available. Because each read contributes a little to the QUAL score, variants in regions with deep coverage can have artificially inflated QUAL scores, giving the impression that the call is supported by more evidence than it really is. To compensate for this, we normalize the variant confidence by depth, which gives us a more objective picture of how well supported the call is.
    * FisherStrand(FS): This is the Phred-scaled probability that there is strand bias at the site. Strand Bias tells us whether the alternate allele was seen more or less often on the forward or reverse strand than the reference allele. When there little to no strand bias at the site, the FS value will be close to 0.
    * RMSMappingQuality(MQ): This is the root mean square mapping quality over all the reads at the site. Instead of the average mapping quality of the site, this annotation gives the square root of the average of the squares of the mapping qualities at the site. It is meant to include the standard deviation of the mapping qualities. Including the standard deviation allows us to include the variation in the dataset. A low standard deviation means the values are all close to the mean, whereas a high standard deviation means the values are all far from the mean.When the mapping qualities are good at a site, the MQ will be around 60.
    * MappingQualityRankSumTest(MQRankSum):This variant-level annotation compares the mapping qualities of the reads supporting the reference allele with those supporting the alternate allele. The ideal result is a value close to zero, which indicates there is little to no difference. A negative value indicates that the reads supporting the alternate allele have lower mapping quality scores than those supporting the reference allele. Conversely, a positive value indicates that the reads supporting the alternate allele have higher mapping quality scores than those supporting the reference allele.


  ```
  $JAVA8 -jar $GATK \
  -T VariantFiltration \
  -R $REF \
  -o $VCFDIR/$SAMPLEID.HC.SNP.filtered.vcf \
  --variant $VCFDIR/$SAMPLEID.HC.vcf \
  --filterExpression "QD < 2.0 || FS > 60.0 || MQ < 40.0 || MQRankSum < -12.5 || ReadPosRankSum < -8.0" \
  --filterName "lowQual"
  ```

  Step3: Extract INDEL from the raw call subset
  ```
  $JAVA8 -jar $GATK \
  -T SelectVariants \
  -R $REF \
  -V $VCFDIR/$SAMPLEID.HC.vcf \
  -selectType INDEL  \
  -o $VCFDIR/$SAMPLEID.HC.INDEL.vcf
  ```

  Step4: Apply filter parameters for INDELs.
  ```
  $JAVA8 -jar $GATK \
  -T VariantFiltration \
  -R $REF \
  -o $VCFDIR/$SAMPLEID.HC.INDEL.filtered.vcf \
  --variant $VCFDIR/$SAMPLEID.HC.INDEL.vcf \
  --filterExpression "QD < 2.0 || FS > 200.0 || ReadPosRankSum < -20.0" \
  --filterName "lowQual"
  ```
